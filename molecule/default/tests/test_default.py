import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('alarm_config_logger')


def test_service(host):
    service = host.service("alarm-config-logger@TEST")
    assert service.is_enabled
    assert service.is_running
